package com.example.electionapp.BeenClasses;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.electionapp.app.App;

public class SharedPreferance {

    /*SharedPreferences sharedPreferences,mSharedPreferences;
    SharedPreferences.Editor editor;
    Context context;*/

    /*public SharedPreferance(Context context) {
        this.context = context;
    }*/

    public static void setLoggedIn(boolean loggedIn)
    {
        //sharedPreferences = context.getSharedPreferences("loginPage",context.MODE_PRIVATE);
        //editor = sharedPreferences.edit();
        App.getEditor().putBoolean("loggedInMode",loggedIn);
        App.getEditor().commit();
    }

    public static Boolean loggedIn()
    {
        //sharedPreferences = context.getSharedPreferences("loginPage",context.MODE_PRIVATE);
        return App.getSharedPreferences().getBoolean("loggedInMode",false);
    }

    public static void setProjectId(int projectId){
        //mSharedPreferences = context.getSharedPreferences("Project_id", context.MODE_PRIVATE); // 0 - for private mode
        //SharedPreferences.Editor editor1 = mSharedPreferences.edit();
        App.getEditor().putInt("ProjectId", projectId);
        App.getEditor().commit();
    }

    public static int getProjectId(){
        //mSharedPreferences = context.getSharedPreferences("Project_id", context.MODE_PRIVATE);
        int i = App.getSharedPreferences().getInt("ProjectId",0);
        return i;
    }

    public static void setProjectName(String name){
        App.getEditor().putString("ProjectName",name);
        App.getEditor().commit();
    }

    public static String getProjectName(){
        String name = App.getSharedPreferences().getString("ProjectName","");
        return name;
    }

    public static void setProjectTagline(String tagline){
        App.getEditor().putString("ProjectTagline",tagline);
        App.getEditor().commit();
    }

    public static String getProjectTagline(){
        String tagline = App.getSharedPreferences().getString("ProjectTagline","");
        return tagline;
    }
}
