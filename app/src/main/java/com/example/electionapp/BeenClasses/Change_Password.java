package com.example.electionapp.BeenClasses;

public class Change_Password {

    /**
     * msg : 2
     * Member_id : 1
     */
    private int msg;
    private String Member_id;

    public int getMsg() {
        return msg;
    }

    public void setMsg(int msg) {
        this.msg = msg;
    }

    public String getMember_id() {
        return Member_id;
    }

    public void setMember_id(String Member_id) {
        this.Member_id = Member_id;
    }
}
