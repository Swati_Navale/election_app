package com.example.electionapp.BeenClasses;

import java.util.List;

public class State {


    /**
     * success : 1
     * state : [{"state_id":"1","country_id":"1","state_name":"maharastra"},{"state_id":"2","country_id":"1","state_name":"gujrat"}]
     */

    private int success;
    private List<StateBean> state;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public List<StateBean> getState() {
        return state;
    }

    public void setState(List<StateBean> state) {
        this.state = state;
    }

    public static class StateBean {
        /**
         * state_id : 1
         * country_id : 1
         * state_name : maharastra
         */

        private String state_id;
        private String country_id;
        private String state_name;

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getCountry_id() {
            return country_id;
        }

        public void setCountry_id(String country_id) {
            this.country_id = country_id;
        }

        public String getState_name() {
            return state_name;
        }

        public void setState_name(String state_name) {
            this.state_name = state_name;
        }
    }
}
