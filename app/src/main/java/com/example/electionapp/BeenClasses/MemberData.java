package com.example.electionapp.BeenClasses;

public class MemberData {


    /**
     * command : R
     * fullName : Lemborgeni
     * motherName : LaraDikosta
     * dateOfBirth : 1992-05-08
     * gender : Male
     * emailId : Dekosta@gmail.com
     * mobileNumber1 : 8989895656
     * mobileNumber2 : 4512457856
     * adharNumber : 8574123652
     * panNumber : P895
     * castId : 1
     * roleId : 1
     * username : testUser
     * password : 12345
     * occupation : HOTEL MGT
     * projectId : 1
     * address : {"state_id":1,"taluka_id":1,"dist_id":5,"country_id":1,"zipcode":"526985","addressline1":"ADRESSS","village":"Ans"}
     */

    private String command;
    private String fullName;
    private String motherName;
    private String dateOfBirth;
    private String age;
    private String gender;
    private String emailId;
    private String mobileNumber1;
    private String mobileNumber2;
    private String adharNumber;
    private String panNumber;
    private int castId;
    private int roleId;
    private String username;
    private String password;
    private String occupation;
    private String projectId;
    private AddressBean address;

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMobileNumber1() {
        return mobileNumber1;
    }

    public void setMobileNumber1(String mobileNumber1) {
        this.mobileNumber1 = mobileNumber1;
    }

    public String getMobileNumber2() {
        return mobileNumber2;
    }

    public void setMobileNumber2(String mobileNumber2) {
        this.mobileNumber2 = mobileNumber2;
    }

    public String getAdharNumber() {
        return adharNumber;
    }

    public void setAdharNumber(String adharNumber) {
        this.adharNumber = adharNumber;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public int getCastId() {
        return castId;
    }

    public void setCastId(int castId) {
        this.castId = castId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public AddressBean getAddress() {
        return address;
    }

    public void setAddress(AddressBean address) {
        this.address = address;
    }

    public static class AddressBean {
        /**
         * state_id : 1
         * taluka_id : 1
         * dist_id : 5
         * country_id : 1
         * zipcode : 526985
         * addressline1 : ADRESSS
         * village : Ans
         */

        private int state_id;
        private int taluka_id;
        private int dist_id;
        private int country_id;
        private String zipcode;
        private String addressline1;
        private String village;

        public int getState_id() {
            return state_id;
        }

        public void setState_id(int state_id) {
            this.state_id = state_id;
        }

        public int getTaluka_id() {
            return taluka_id;
        }

        public void setTaluka_id(int taluka_id) {
            this.taluka_id = taluka_id;
        }

        public int getDist_id() {
            return dist_id;
        }

        public void setDist_id(int dist_id) {
            this.dist_id = dist_id;
        }

        public int getCountry_id() {
            return country_id;
        }

        public void setCountry_id(int country_id) {
            this.country_id = country_id;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getAddressline1() {
            return addressline1;
        }

        public void setAddressline1(String addressline1) {
            this.addressline1 = addressline1;
        }

        public String getVillage() {
            return village;
        }

        public void setVillage(String village) {
            this.village = village;
        }
    }
}
