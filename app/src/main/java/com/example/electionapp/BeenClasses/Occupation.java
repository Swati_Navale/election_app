package com.example.electionapp.BeenClasses;

import java.util.List;

public class Occupation {


    /**
     * success : 1
     * occupations : [{"occupation":"Teacher"},{"occupation":"Technologist"},{"occupation":"Technician"}]
     */

    private int success;
    private List<OccupationsBean> occupations;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public List<OccupationsBean> getOccupations() {
        return occupations;
    }

    public void setOccupations(List<OccupationsBean> occupations) {
        this.occupations = occupations;
    }

    public static class OccupationsBean {
        /**
         * occupation : Teacher
         */

        private String occupation;

        public String getOccupation() {
            return occupation;
        }

        public void setOccupation(String occupation) {
            this.occupation = occupation;
        }
    }
}
