package com.example.electionapp.BeenClasses;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

    public class MySingleton {

        private static MySingleton mySingleton;
        private RequestQueue requestQueue;
        private static Context context;

        private MySingleton(Context mContext)
        {
            context = mContext;
            requestQueue = getRequestQueue();

        }

        private RequestQueue getRequestQueue() {
            if(requestQueue==null)
            {
                requestQueue = Volley.newRequestQueue(context.getApplicationContext());
            }
            return requestQueue;
        }

        public static synchronized MySingleton getInstance(Context mcontext)
        {
            if(mySingleton==null)
            {
                mySingleton=new MySingleton(mcontext);
            }
            return mySingleton;
        }

        public<T> void addToRequestQueue(Request<T> request)
        {
            getRequestQueue().add(request);
        }
    }
