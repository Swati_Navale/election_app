package com.example.electionapp.BeenClasses;

public class OccupationListData {
    private String occupation_name;
    private int imgId;
    public OccupationListData(String oocup_name, int imgId) {
        this.occupation_name = oocup_name;
        this.imgId = imgId;
    }
    public String getDescription() {
        return occupation_name;
    }
    public void setDescription(String occup_name) {
        this.occupation_name = occup_name;
    }
    public int getImgId() {
        return imgId;
    }
    public void setImgId(int imgId) {
        this.imgId = imgId;
    }
}
