package com.example.electionapp.BeenClasses;

public class UserRegisterationResponse {

    /**
     * mgs : Sucessfuly User registerd
     * msgcode : 200
     */

    private String mgs;
    private int msgcode;

    public String getMgs() {
        return mgs;
    }

    public void setMgs(String mgs) {
        this.mgs = mgs;
    }

    public int getMsgcode() {
        return msgcode;
    }

    public void setMsgcode(int msgcode) {
        this.msgcode = msgcode;
    }
}
