package com.example.electionapp.BeenClasses;

import java.util.List;

public class District {


    /**
     * success : 1
     * dist : [{"dist_id":"5","state_id":"1","dist_name":"pune"},{"dist_id":"6","state_id":"1","dist_name":"beed"},{"dist_id":"7","state_id":"1","dist_name":"latur"}]
     */

    private int success;
    private List<DistBean> dist;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public List<DistBean> getDist() {
        return dist;
    }

    public void setDist(List<DistBean> dist) {
        this.dist = dist;
    }

    public static class DistBean {
        /**
         * dist_id : 5
         * state_id : 1
         * dist_name : pune
         */

        private String dist_id;
        private String state_id;
        private String dist_name;

        public String getDist_id() {
            return dist_id;
        }

        public void setDist_id(String dist_id) {
            this.dist_id = dist_id;
        }

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getDist_name() {
            return dist_name;
        }

        public void setDist_name(String dist_name) {
            this.dist_name = dist_name;
        }
    }
}
