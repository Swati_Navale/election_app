package com.example.electionapp.BeenClasses;

import java.util.List;

public class Address {


    /**
     * success : 1
     * address : [{"address_id":"6","address_type_id":"1","taluka_id":"10","dist_id":"7","country_id":"1","state_id":"1","zipcode":"411016","addressline1":"zmksalz jsx xjsx","village":"ausa","m_id":"4"},{"address_id":"7","address_type_id":"1","taluka_id":"84","dist_id":"6","country_id":"1","state_id":"1","zipcode":"414205","addressline1":"cjdcc cjice  cedjciecj","village":"beed","m_id":"2"},{"address_id":"8","address_type_id":"1","taluka_id":"1","dist_id":"5","country_id":"1","state_id":"1","zipcode":"411017","addressline1":"cjncc odkjciojc ecjkoic","village":"kalewadi","m_id":"1"},{"address_id":"9","address_type_id":"1","taluka_id":"3","dist_id":"7","country_id":"1","state_id":"1","zipcode":"411020","addressline1":"dcndc dcjdcoi ccewcjewc","village":"njndcc","m_id":"5"},{"address_id":"10","address_type_id":"1","taluka_id":"10","dist_id":"7","country_id":"1","state_id":"1","zipcode":"411020","addressline1":"drjehfref fdew fefrehf ","village":"gdefgf","m_id":"2"},{"address_id":"11","address_type_id":"1","taluka_id":"1","dist_id":"5","country_id":"1","state_id":"1","zipcode":"411016","addressline1":"dfndkn fdnfkjref r frfre","village":"vkfdkvf","m_id":"3"},{"address_id":"13","address_type_id":"1","taluka_id":"1","dist_id":"5","country_id":"1","state_id":"1","zipcode":"411016","addressline1":"dfndkn fdnfkjref r frfre","village":"vkfdkvf","m_id":"5"}]
     */

    private int success;
    private List<AddressBean> address;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public List<AddressBean> getAddress() {
        return address;
    }

    public void setAddress(List<AddressBean> address) {
        this.address = address;
    }

    public static class AddressBean {
        /**
         * address_id : 6
         * address_type_id : 1
         * taluka_id : 10
         * dist_id : 7
         * country_id : 1
         * state_id : 1
         * zipcode : 411016
         * addressline1 : zmksalz jsx xjsx
         * village : ausa
         * m_id : 4
         */

        private String address_id;
        private String address_type_id;
        private String taluka_id;
        private String dist_id;
        private String country_id;
        private String state_id;
        private String zipcode;
        private String addressline1;
        private String village;
        private String m_id;

        public String getAddress_id() {
            return address_id;
        }

        public void setAddress_id(String address_id) {
            this.address_id = address_id;
        }

        public String getAddress_type_id() {
            return address_type_id;
        }

        public void setAddress_type_id(String address_type_id) {
            this.address_type_id = address_type_id;
        }

        public String getTaluka_id() {
            return taluka_id;
        }

        public void setTaluka_id(String taluka_id) {
            this.taluka_id = taluka_id;
        }

        public String getDist_id() {
            return dist_id;
        }

        public void setDist_id(String dist_id) {
            this.dist_id = dist_id;
        }

        public String getCountry_id() {
            return country_id;
        }

        public void setCountry_id(String country_id) {
            this.country_id = country_id;
        }

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getAddressline1() {
            return addressline1;
        }

        public void setAddressline1(String addressline1) {
            this.addressline1 = addressline1;
        }

        public String getVillage() {
            return village;
        }

        public void setVillage(String village) {
            this.village = village;
        }

        public String getM_id() {
            return m_id;
        }

        public void setM_id(String m_id) {
            this.m_id = m_id;
        }
    }
}
