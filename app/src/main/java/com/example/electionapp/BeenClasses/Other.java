package com.example.electionapp.BeenClasses;

public class Other {

    /*
    class StateTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String url = "http://192.168.0.107:8080/election_db/state.php";

            try {
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                request.setURI(new URI(url));

                HttpResponse response = client.execute(request);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuffer stringBuffer = new StringBuffer("");
                String line = "";
                while ((line = reader.readLine()) != null) {
                    stringBuffer.append(line);
                    break;
                }
                reader.close();
                result = stringBuffer.toString();
            } catch (Exception e) {
                return new String("There is Exception : " + e.getMessage());
            }
            return result;
        }

        protected void onPostExecute(String result) {
            try {
                statelist.add("Select State");
                JSONObject jsonObject = new JSONObject(result);
                int success = jsonObject.getInt("success");
                if (success == 1) {
                    JSONArray array = jsonObject.getJSONArray("state");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject state = array.getJSONObject(i);
                        String s = state.getString("state_name")+"("+state.getInt("state_id")+")";
                        statelist.add(s);
                    }

                    stateAdapter.notifyDataSetChanged();

                } else {
                    Toast.makeText(getApplicationContext(), "There is no states", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

    }

    class DistTask extends AsyncTask<String, String, String> {

        ArrayList<String> list = new ArrayList<>();

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String url = "http://192.168.0.107:8080/election_db/district.php";

            try {
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                request.setURI(new URI(url));

                HttpResponse response = client.execute(request);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuffer stringBuffer = new StringBuffer("");
                String line = "";
                while ((line = reader.readLine()) != null) {
                    stringBuffer.append(line);
                    break;
                }
                reader.close();
                result = stringBuffer.toString();
            } catch (Exception e) {
                return new String("There is Exception : " + e.getMessage());
            }
            return result;
        }

        protected void onPostExecute(String result) {
            try {
                distlist.add("Select District");
                JSONObject jsonObject = new JSONObject(result);
                int success = jsonObject.getInt("success");
                if (success == 1) {
                    JSONArray array = jsonObject.getJSONArray("dist");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject dist = array.getJSONObject(i);
                        String s = dist.getString("dist_name")+"("+dist.getInt("dist_id")+")";
                        distlist.add(s);
                    }

                    distAdapter.notifyDataSetChanged();

                } else {
                    Toast.makeText(getApplicationContext(), "There is no districts", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

    }

    class TalukaTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String url = "http://192.168.0.107:8080/election_db/taluka.php";

            try {
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                request.setURI(new URI(url));

                HttpResponse response = client.execute(request);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuffer stringBuffer = new StringBuffer("");
                String line = "";
                while ((line = reader.readLine()) != null) {
                    stringBuffer.append(line);
                    break;
                }
                reader.close();
                result = stringBuffer.toString();
            } catch (Exception e) {
                return new String("There is Exception : " + e.getMessage());
            }
            return result;
        }

        protected void onPostExecute(String result) {
            try {
                talukalist.add("Select Taluka");
                JSONObject jsonObject = new JSONObject(result);
                int success = jsonObject.getInt("success");
                if (success == 1) {
                    JSONArray array = jsonObject.getJSONArray("taluka");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject taluka = array.getJSONObject(i);
                        String s = taluka.getString("taluka_name")+"("+taluka.getInt("taluka_id")+")";
                        talukalist.add(s);
                    }

                    talukaAdapter.notifyDataSetChanged();

                } else {
                    Toast.makeText(getApplicationContext(), "There is no talukas", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

    }*/
}
