package com.example.electionapp.BeenClasses;

public class Check_Otp {
    /**
     * msg : 1
     * Mobile_Number : 8379939930
     */

    private int msg;
    private String Mobile_Number;

    public int getMsg() {
        return msg;
    }

    public void setMsg(int msg) {
        this.msg = msg;
    }

    public String getMobile_Number() {
        return Mobile_Number;
    }

    public void setMobile_Number(String Mobile_Number) {
        this.Mobile_Number = Mobile_Number;
    }
}
