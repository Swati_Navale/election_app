package com.example.electionapp.BeenClasses;

public class MobileOtp {


    /**
     * msg : 1
     * UserOTP : 8480
     * MobileNumber : 8379939930
     * username : swatibahir@gmail.com
     */

    private String msg;
    private String UserOTP;
    private String MobileNumber;
    private String username;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getUserOTP() {
        return UserOTP;
    }

    public void setUserOTP(String UserOTP) {
        this.UserOTP = UserOTP;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String MobileNumber) {
        this.MobileNumber = MobileNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}