package com.example.electionapp.BeenClasses;

import java.util.List;

public class Taluka {

    private int success;
    private List<TalukaBeen> taluka;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public List<TalukaBeen> getTaluka() {

        return taluka;
    }

    public void setTaluka(List<TalukaBeen> taluka) {
        this.taluka = taluka;
    }

    public static class TalukaBeen {

        private String taluka_id;
        private String dist_id;
        private String taluka_name;

        public String getTaluka_id() {
            return taluka_id;
        }

        public void setTaluka_id(String taluka_id) {
            this.taluka_id = taluka_id;
        }

        public String getDist_id() {
            return dist_id;
        }

        public void setDist_id(String dist_id) {
            this.dist_id = dist_id;
        }

        public String getTaluka_name() {
            return taluka_name;
        }

        public void setTaluka_name(String taluka_name) {
            this.taluka_name = taluka_name;
        }
    }
}
