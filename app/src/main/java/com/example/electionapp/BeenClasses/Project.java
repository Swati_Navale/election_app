package com.example.electionapp.BeenClasses;

import java.util.List;

public class Project {

    /**
     * success : 1
     * project : [{"project_id":"1","project_name":"com.example.electionapp","cast_name":"kumbhar","sub_cast":"kumbhar"}]
     */

    private int success;
    private List<ProjectBean> project;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public List<ProjectBean> getProject() {
        return project;
    }

    public void setProject(List<ProjectBean> project) {
        this.project = project;
    }

    public static class ProjectBean {
        /**
         * project_id : 1
         * project_name : com.example.electionapp
         * cast_name : kumbhar
         * sub_cast : kumbhar
         */

        private String project_id;
        private String project_name;
        private String tagline;
        private String pakg_name;
        private String cast_name;
        private String sub_cast;

        public String getProject_id() {
            return project_id;
        }

        public void setProject_id(String project_id) {
            this.project_id = project_id;
        }

        public String getProject_name() {
            return project_name;
        }

        public void setProject_name(String project_name) {
            this.project_name = project_name;
        }

        public String getCast_name() {
            return cast_name;
        }

        public void setCast_name(String cast_name) {
            this.cast_name = cast_name;
        }

        public String getSub_cast() {
            return sub_cast;
        }

        public void setSub_cast(String sub_cast) {
            this.sub_cast = sub_cast;
        }

        public String getTagline() {
            return tagline;
        }

        public void setTagline(String tagline) {
            this.tagline = tagline;
        }

        public String getPakg_name() {
            return pakg_name;
        }

        public void setPakg_name(String pakg_name) {
            this.pakg_name = pakg_name;
        }
    }
}
