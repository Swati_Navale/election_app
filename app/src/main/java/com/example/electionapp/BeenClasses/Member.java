package com.example.electionapp.BeenClasses;

import java.util.List;

public class Member {


    /**
     * success : 1
     * members : [{"m_id":"1","full_name":"Swati Kishor Navale","mother_name":"Sindhu","gender":"female","date_of_birth":"2019-10-09","age":"26","email_id":"swatibahir@gmail.com","mobile_number1":"8379939930","mobile_number2":"9960996089","aadhar_number":"333344445555","pan_number":"CCHI36473","cast_id":"2","role_id":"4","occupation":"job","createdOn":"2019-11-21","updatedOn":"2019-11-21"}]
     */

    private int success;
    private List<MembersBean> members;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public List<MembersBean> getMembers() {
        return members;
    }

    public void setMembers(List<MembersBean> members) {
        this.members = members;
    }

    public static class MembersBean {
        /**
         * m_id : 1
         * full_name : Swati Kishor Navale
         * mother_name : Sindhu
         * gender : female
         * date_of_birth : 2019-10-09
         * age : 26
         * email_id : swatibahir@gmail.com
         * mobile_number1 : 8379939930
         * mobile_number2 : 9960996089
         * aadhar_number : 333344445555
         * pan_number : CCHI36473
         * cast_id : 2
         * role_id : 4
         * occupation : job
         * createdOn : 2019-11-21
         * updatedOn : 2019-11-21
         */

        private String m_id;
        private String full_name;
        private String mother_name;
        private String gender;
        private String date_of_birth;
        private String age;
        private String email_id;
        private String mobile_number1;
        private String mobile_number2;
        private String aadhar_number;
        private String pan_number;
        private String cast_id;
        private String role_id;
        private String occupation;
        private String createdOn;
        private String updatedOn;

        public String getM_id() {
            return m_id;
        }

        public void setM_id(String m_id) {
            this.m_id = m_id;
        }

        public String getFull_name() {
            return full_name;
        }

        public void setFull_name(String full_name) {
            this.full_name = full_name;
        }

        public String getMother_name() {
            return mother_name;
        }

        public void setMother_name(String mother_name) {
            this.mother_name = mother_name;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getDate_of_birth() {
            return date_of_birth;
        }

        public void setDate_of_birth(String date_of_birth) {
            this.date_of_birth = date_of_birth;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getEmail_id() {
            return email_id;
        }

        public void setEmail_id(String email_id) {
            this.email_id = email_id;
        }

        public String getMobile_number1() {
            return mobile_number1;
        }

        public void setMobile_number1(String mobile_number1) {
            this.mobile_number1 = mobile_number1;
        }

        public String getMobile_number2() {
            return mobile_number2;
        }

        public void setMobile_number2(String mobile_number2) {
            this.mobile_number2 = mobile_number2;
        }

        public String getAadhar_number() {
            return aadhar_number;
        }

        public void setAadhar_number(String aadhar_number) {
            this.aadhar_number = aadhar_number;
        }

        public String getPan_number() {
            return pan_number;
        }

        public void setPan_number(String pan_number) {
            this.pan_number = pan_number;
        }

        public String getCast_id() {
            return cast_id;
        }

        public void setCast_id(String cast_id) {
            this.cast_id = cast_id;
        }

        public String getRole_id() {
            return role_id;
        }

        public void setRole_id(String role_id) {
            this.role_id = role_id;
        }

        public String getOccupation() {
            return occupation;
        }

        public void setOccupation(String occupation) {
            this.occupation = occupation;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }
    }
}
