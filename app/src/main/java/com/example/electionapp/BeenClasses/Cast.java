package com.example.electionapp.BeenClasses;

import java.util.List;

public class Cast {


    /**
     * success : 1
     * cast : [{"cast_id":"1","name":"Kumbhar"},{"cast_id":"2","name":"Open"},{"cast_id":"3","name":"OBC"},{"cast_id":"4","name":"General"},{"cast_id":"5","name":"SC"},{"cast_id":"6","name":"ST"},{"cast_id":"7","name":"SBC"},{"cast_id":"8","name":"VJ"},{"cast_id":"9","name":"NT-B"},{"cast_id":"10","name":"NT-C"},{"cast_id":"11","name":"NT-D"}]
     */

    private int success;
    private List<CastBean> cast;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public List<CastBean> getCast() {
        return cast;
    }

    public void setCast(List<CastBean> cast) {
        this.cast = cast;
    }

    public static class CastBean {
        /**
         * cast_id : 1
         * name : Kumbhar
         */

        private String cast_id;
        private String name;

        public String getCast_id() {
            return cast_id;
        }

        public void setCast_id(String cast_id) {
            this.cast_id = cast_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
