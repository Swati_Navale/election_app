package com.example.electionapp.Activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.electionapp.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.sql.Connection;

public class MemberDetailsActivity extends AppCompatActivity {

    TextView tvFullName,tvMotherName,tvGender,tvDateOfBirth,tvAge,tvEmail,tvMobile1,tvMobile2,tvAadharNo,tvPanNo,tvOccupation,
            tvCast,tvRole,tvCountry,tvState,tvDistrict,tvTaluka,tvVillage,tvAddressline,tvZipcode,tvOnCreated,tvOnUpdated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Toolbar toolbar = findViewById(R.id.custom_bar);
        TextView toolbarTitle = findViewById(R.id.titleText);
        toolbar.setTitle("");
        toolbarTitle.setText("Member Details");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvFullName = findViewById(R.id.nameTv);
        tvMotherName = findViewById(R.id.motherNameTv);
        tvGender = findViewById(R.id.genderTv);
        tvDateOfBirth = findViewById(R.id.dobTv);
        tvEmail = findViewById(R.id.emailTv);
        tvMobile1 = findViewById(R.id.number1Tv);
        tvMobile2 = findViewById(R.id.number2Tv);
        tvAadharNo = findViewById(R.id.aadharTv);
        tvPanNo = findViewById(R.id.panTv);
        tvOccupation = findViewById(R.id.occupationTv);
        tvCast = findViewById(R.id.castTv);
        tvRole = findViewById(R.id.roleTv);
        tvCountry = findViewById(R.id.countryTv);
        tvState = findViewById(R.id.stateTv);
        tvDistrict = findViewById(R.id.districtTv);
        tvTaluka = findViewById(R.id.talukaTv);
        tvVillage = findViewById(R.id.villageTv);
        tvAddressline = findViewById(R.id.addresslineTv);
        tvZipcode = findViewById(R.id.zipcodeTv);
        tvOnCreated = findViewById(R.id.createdTv);
        tvOnUpdated = findViewById(R.id.updatedTv);

        Intent intent = getIntent();

        String Name = intent.getStringExtra("sName");
        String MName = intent.getStringExtra("sMName");
        String Gender = intent.getStringExtra("sGender");
        String Dob = intent.getStringExtra("sDob");
        String Email = intent.getStringExtra("sEmail");
        String Mob1 = intent.getStringExtra("sMob1");
        String Mob2 = intent.getStringExtra("sMob2");
        String Aadhar = intent.getStringExtra("sAadhar");
        String Pan = intent.getStringExtra("sPan");
        String Occupation = intent.getStringExtra("sOccupation");
        String Cast = intent.getStringExtra("sCast");
        String Role = intent.getStringExtra("sRole");
        String Created = intent.getStringExtra("sCreated");
        String Updated = intent.getStringExtra("sUpdated");



        tvFullName.setText(Name);
       /* tvGender.setText(Gender);
        tvMotherName.setText(MName);
       */ tvDateOfBirth.setText(Dob);
        tvEmail.setText(Email);
        tvMobile1.setText(Mob1);
        tvMobile2.setText(Mob2);
       /* tvAadharNo.setText("Aadhar Number : "+Aadhar);
        tvPanNo.setText("Pan Number : "+Pan);
       */ tvOccupation.setText(Occupation);
        tvCast.setText("Kumbhar");
       /* tvRole.setText("Role : "+Role);
       tvOnCreated.setText("OnCreated : "+Created);
        tvOnUpdated.setText("OnUpdated : "+Updated);
*/
       tvAddressline.setText("Pune");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
//      //  listView = findViewById(R.id.listView);
//        arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);
//        listView.setAdapter(arrayAdapter);
//        new Connection().execute();
//    }
//
//    class Connection extends AsyncTask<String, String, String> {
//        @Override
//        protected String doInBackground(String... strings) {
//            String result = "";
//            String url = "http://servicepartnerind.com/json/castesurvay/";
//           // String url = "http://192.168.0.102:8080/election_db/member_list.php";
//            try {
//                HttpClient client = new DefaultHttpClient();
//                HttpGet request = new HttpGet();
//                request.setURI(new URI(url));
//
//                HttpResponse response = client.execute(request);
//                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//
//                StringBuffer stringBuffer = new StringBuffer("");
//                String line = "";
//                while ((line = reader.readLine()) != null) {
//                    stringBuffer.append(line);
//                    break;
//                }
//                reader.close();
//                result = stringBuffer.toString();
//            } catch (Exception e) {
//                return new String("There is Exception : " + e.getMessage());
//            }
//            return result;
//        }
//
//        protected void onPostExecute(String result) {
//           // Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
//            try
//            {
//                JSONObject jsonObject = new JSONObject(result);
//                int success = jsonObject.getInt("success");
//                if(success == 1)
//                {
//                    JSONArray members = jsonObject.getJSONArray("members");
//                    for(int i=0; i < members.length(); i++)
//                    {
//                        JSONObject member = members.getJSONObject(i);
//                        int m_id = member.getInt("m_id");
//                        String fname = member.getString("first_name");
//                        String mname = member.getString("middel_name");
//                        String lname = member.getString("last_name");
//                        String gender = member.getString("gender");
//                        String dob = member.getString("date_of_birth");
//                        int age = member.getInt("age");
//                        String email = member.getString("email_id");
//                        String mobile1 = member.getString("mobile_number1");
//                        String mobile2 = member.getString("mobile_number2");
//                        String aadhar = member.getString("aadhar_number");
//                        String pan = member.getString("pan_number");
//                        String currentAddress = member.getString("residential_address");
//                       // String PAddress = member.getString("permanant_address");
//                        int c_id = member.getInt("cast_id");
//                        int r_id = member.getInt("role_id");
//                        String occupation = member.getString("occupation");
//                        String onCreate = member.getString("createdOn");
//                        String onUpdate = member.getString("updatedOn");
//
//                        String line = "id : " + m_id + "\nFirst Name : " + fname + "\nMiddel Name : " + mname + "\nLast Name : " + lname
//                                + "\nGender : " + gender + "\nDob : " + dob + "\nAge : " + age + "\nEmail : " + email
//                                + "\nMobile_1 : " + mobile1 + "\nMobile_2 : " + mobile2 + "\nAadhar Number : " + aadhar
//                                + "\nPan Number : " + pan + "\nC_Address : " + currentAddress + "\nCast id : " + c_id
//                                + "\nRole id : " + r_id + "\nOccupation : " + occupation + "\nCreatedOn : " + onCreate + "\nUpdatedOn : " + onUpdate;
//                        arrayAdapter.add(line);
//                    }
//                }else
//                {
//                    Toast.makeText(getApplicationContext(), "There is no members", Toast.LENGTH_SHORT).show();
//                }
//            } catch (JSONException e) {
//                Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
//            }
//        }
//    }
//}
