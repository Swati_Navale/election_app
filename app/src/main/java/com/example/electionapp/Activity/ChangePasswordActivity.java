package com.example.electionapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.electionapp.BeenClasses.Change_Password;
import com.example.electionapp.BeenClasses.Occupation;
import com.example.electionapp.Interface.ApiClient;
import com.example.electionapp.Interface.ApiInterface;
import com.example.electionapp.R;
import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {

    public TextInputEditText tIvnew_password,tIvRetypedPassword;
    Button btnChangePassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        Toolbar toolbar = findViewById(R.id.custom_bar);
        TextView toolbarTitle = findViewById(R.id.titleText);
        toolbar.setTitle("");
        toolbarTitle.setText("Change Password");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tIvnew_password = findViewById(R.id.new_password);
        tIvRetypedPassword = findViewById(R.id.con_password);
        btnChangePassword = findViewById(R.id.changePassBtn);

        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newPass = tIvnew_password.getText().toString();
                String conPass = tIvRetypedPassword.getText().toString();

                if(newPass.equals(conPass))
                {
                    changePassword(newPass);
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"plz enter same password in both feild",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void changePassword(String newPass) {

        Intent intent = getIntent();
        String contact = intent.getStringExtra("MobileNumber");
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Change_Password> call_member = apiInterface.changePassword(newPass,contact);
        call_member.enqueue(new Callback<Change_Password>() {
            @Override
            public void onResponse(Call<Change_Password> call, Response<Change_Password> response) {
                Change_Password change_password = response.body();

                int msg = change_password.getMsg();
                if (msg==2) {
                    Toast.makeText(getApplicationContext(), "your password is changed successfully", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "somethig wrong to change your password", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Change_Password> call, Throwable t) {

            }
        });
    }
}
