package com.example.electionapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.electionapp.BeenClasses.Change_Password;
import com.example.electionapp.BeenClasses.Project;
import com.example.electionapp.BeenClasses.SharedPreferance;
import com.example.electionapp.BeenClasses.User;
import com.example.electionapp.Interface.ApiClient;
import com.example.electionapp.Interface.ApiInterface;
import com.example.electionapp.R;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    TextInputEditText tInUsername,tInPassword;
    Button btnLogin;
    CheckBox checkBoxShowPassword;
    TextView tvForgotPassword,tvRegister;
    SharedPreferance sharedPreferance;
    int  project_id=0;
    String name;

    List<Project.ProjectBean> projectBeanArrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tInUsername = findViewById(R.id.username);
        tInPassword = findViewById(R.id.password);
        btnLogin = findViewById(R.id.loginBtn);
        checkBoxShowPassword = findViewById(R.id.show_hide_password);
        tvForgotPassword = findViewById(R.id.forgot_password);
        tvRegister = findViewById(R.id.createAccount);

        String packagename = getPackageName();
        getProjectDetails(packagename);
        if(SharedPreferance.loggedIn())
        {
            Intent intent = new Intent(LoginActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        }

        checkBoxShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    tInPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }else
                {
                    tInPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this,ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, MemberRegistrationActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void login()
    {
        String username = tInUsername.getText().toString();
        String password = tInPassword.getText().toString();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<User> call_member = apiInterface.checkUserPassword(username,password);
        call_member.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();

                int msg = user.getSuccess();
                if (msg==1) {
                    SharedPreferance.setLoggedIn(true);
                   // Toast.makeText(getApplicationContext(), "login successfully", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "wrong username and password", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

    private void getProjectDetails(String pakg_name) {
       ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Project> call_project = apiInterface.getProject(pakg_name);
        call_project.enqueue(new Callback<Project>() {
            @Override
            public void onResponse(Call<Project> call, Response<Project> response) {
                Project project = response.body();

                int msg = project.getSuccess();
                if (msg == 1) {
                    projectBeanArrayList = project.getProject();
                    Project.ProjectBean projectBean = projectBeanArrayList.get(0);
                    Log.e("Project Id-----------"," "+projectBean.getProject_id());
                     project_id = Integer.parseInt(projectBean.getProject_id());
                     name = projectBean.getProject_name();
                     SharedPreferance.setProjectName(name);
                     SharedPreferance.setProjectTagline(projectBean.getTagline());
                     SharedPreferance.setProjectId(project_id);

                } else {
                    Toast.makeText(LoginActivity.this, "Project not found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Project> call, Throwable t) {

            }
        });
    }
}
