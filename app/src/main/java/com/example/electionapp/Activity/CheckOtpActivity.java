package com.example.electionapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.electionapp.BeenClasses.Check_Otp;
import com.example.electionapp.BeenClasses.MobileOtp;
import com.example.electionapp.Interface.ApiClient;
import com.example.electionapp.Interface.ApiInterface;
import com.example.electionapp.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckOtpActivity extends AppCompatActivity {

    TextView tvResend;
    Button btnCheck;
    EditText edOtp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_otp);

        edOtp = findViewById(R.id.edOtp);
        btnCheck = findViewById(R.id.checkBtn);
        tvResend = findViewById(R.id.tvResend);

        Toolbar toolbar = findViewById(R.id.custom_bar);
        TextView toolbarTitle = findViewById(R.id.titleText);
        toolbar.setTitle("");
        toolbarTitle.setText("Check Otp");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkOtp();
            }
        });
        
        tvResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reSendOTP();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void reSendOTP() {
        String mobileNumber=getIntent().getStringExtra("MobileNumber");
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<MobileOtp> call_member = apiInterface.getOtp(mobileNumber);
        call_member.enqueue(new Callback<MobileOtp>() {
            @Override
            public void onResponse(Call<MobileOtp> call, Response<MobileOtp> response) {
                MobileOtp mobileOtp = response.body();
                String msg = mobileOtp.getMsg();

                if (msg.equals("1") ) {
                    Toast.makeText(getApplicationContext(), "Send otp on your mobile,plz cheack", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getApplicationContext(), "Error in resend otp", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<MobileOtp> call, Throwable t) {

            }
        });


    }

    private void checkOtp() {
        String otp=edOtp.getText().toString();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Check_Otp> call_otp = apiInterface.getCheckedOtp(otp);
        call_otp.enqueue(new Callback<Check_Otp>() {
            @Override
            public void onResponse(Call<Check_Otp> call, Response<Check_Otp> response) {
                Check_Otp mobileOtp = response.body();
                int msg = mobileOtp.getMsg();

                if (msg==1) {
                    Toast.makeText(CheckOtpActivity.this, "otp is macthed...", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(CheckOtpActivity.this, ChangePasswordActivity.class);
                    String contact = mobileOtp.getMobile_Number();
                    intent.putExtra("MobileNumber",contact);
                    startActivity(intent);
                } else {
                    Toast.makeText(CheckOtpActivity.this, "you entered wrong otp.....!,plz enter correct otp", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Check_Otp> call, Throwable t) {

            }
        });


    }
}
