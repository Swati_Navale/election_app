package com.example.electionapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.electionapp.BeenClasses.MemberDetails;
import com.example.electionapp.BeenClasses.MobileOtp;
import com.example.electionapp.BeenClasses.Occupation;
import com.example.electionapp.BeenClasses.User;
import com.example.electionapp.Interface.ApiClient;
import com.example.electionapp.Interface.ApiInterface;
import com.example.electionapp.R;
import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {

    TextInputEditText tIedMobileNumber,tIedEmail;
    String mobileNumber,Email;
    Button btnEmail,btnMobileNumber;
    LinearLayout mobileLayout,emailLayout;
    RadioGroup radioGroup;

    RadioButton mobileRadioButton,emailRadioButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        Toolbar toolbar = findViewById(R.id.custom_bar);
        TextView toolbarTitle = findViewById(R.id.titleText);
        toolbar.setTitle("");
        toolbarTitle.setText("Forgot Password");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mobileLayout = findViewById(R.id.mobileLinearLayout);
        emailLayout = findViewById(R.id.emailLinearLayout);
        tIedMobileNumber = findViewById(R.id.edMobile);
        tIedEmail = findViewById(R.id.edEmail);
        radioGroup = findViewById(R.id.radioGroup);

        mobileLayout.setVisibility(View.VISIBLE);
        emailLayout.setVisibility(View.GONE);

        btnMobileNumber = findViewById(R.id.submitMobileNo);
        btnMobileNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitMobileNumber();
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mobileRadioButton = group.findViewById(R.id.mobileOtpRbutton);
                emailRadioButton = group.findViewById(R.id.emailRbutton);

                if(mobileRadioButton.isChecked())
                {
                    mobileLayout.setVisibility(View.VISIBLE);
                    emailLayout.setVisibility(View.GONE);

                    btnMobileNumber.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            submitMobileNumber();
                        }
                    });
                }

                if(emailRadioButton.isChecked())
                {
                    mobileLayout.setVisibility(View.GONE);
                    emailLayout.setVisibility(View.VISIBLE);

                    btnEmail = findViewById(R.id.submitEmail);
                    btnEmail.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            submit();
                        }
                    });
                }

            }
        });


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void submitMobileNumber()
    {
        mobileNumber = tIedMobileNumber.getText().toString();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<MobileOtp> call_member = apiInterface.getOtp(mobileNumber);
        call_member.enqueue(new Callback<MobileOtp>() {
            @Override
            public void onResponse(Call<MobileOtp> call, Response<MobileOtp> response) {
                MobileOtp mobileOtp = response.body();
                String msg = mobileOtp.getMsg();

                if (msg.equals("1") ) {
                    Toast.makeText(ForgotPasswordActivity.this, "Send otp on your mobile,plz cheack", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ForgotPasswordActivity.this,CheckOtpActivity.class);
                    intent.putExtra("MobileNumber",mobileNumber);
                    startActivity(intent);

                } else {
                    Toast.makeText(ForgotPasswordActivity.this, "plz enter your register number...", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<MobileOtp> call, Throwable t) {

            }
        });


    }

    public void submit(){
        Email = tIedEmail.getText().toString();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<User> call_member = apiInterface.getEmail(Email);
        call_member.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();
                int msg = user.getSuccess();

                if (msg == 1) {
                    Toast.makeText(ForgotPasswordActivity.this, "Send password to your email_id", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ForgotPasswordActivity.this,LoginActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(ForgotPasswordActivity.this, "error to send password,Plz enter correct email_id", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }
}