package com.example.electionapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.electionapp.Adapter.MemberOccupationAdapter;
import com.example.electionapp.BeenClasses.OccupationWiseMember;
import com.example.electionapp.BeenClasses.SharedPreferance;
import com.example.electionapp.Interface.ApiClient;
import com.example.electionapp.Interface.ApiInterface;
import com.example.electionapp.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemberOccupationActivity extends AppCompatActivity {

    List<OccupationWiseMember.MemberBean> memberRecordList =new ArrayList<>();
    RecyclerView recyclerView;
    MemberOccupationAdapter memberOccupationAdapter;

    int project_id = SharedPreferance.getProjectId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //recyclerView.setVisibility(View.GONE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_occupation);

        Toolbar toolbar = findViewById(R.id.custom_bar);
        TextView toolbarTitle = findViewById(R.id.titleText);
        toolbar.setTitle("");
        toolbarTitle.setText("Election App");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = findViewById(R.id.rMOList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        memberOccupationAdapter = new MemberOccupationAdapter(this,memberRecordList);
        recyclerView.setAdapter(memberOccupationAdapter);

        String occupation_name = getIntent().getStringExtra("getOccupation");
        getMemberList(occupation_name);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void getMemberList(String occupation_name)
    {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OccupationWiseMember> call_member = apiInterface.getOccupationMemberList(occupation_name,Selection_listActivity.taluka_id,project_id);
        call_member.enqueue(new Callback<OccupationWiseMember>()
        {
            @Override
            public void onResponse(Call<OccupationWiseMember> call, Response<OccupationWiseMember> response) {
                try {
                    if(response.code()==200){
                        if(response.body()!=null){
                            OccupationWiseMember member = response.body();
                            int msg = member.getSuccess();
                            memberRecordList.clear();
                            if (msg == 1) {
                                memberRecordList.addAll(member.getMember());
                            } else {
                                Toast.makeText(MemberOccupationActivity.this, "No Member found", Toast.LENGTH_SHORT).show();
                            }
                            memberOccupationAdapter.notifyDataSetChanged();
                        }else {
                            Toast.makeText(getApplicationContext(),"Opps some thing went wrong",Toast.LENGTH_SHORT).show();
                        }
                    }else
                    {
                        Toast.makeText(getApplicationContext(),"not found",Toast.LENGTH_SHORT).show();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<OccupationWiseMember> call, Throwable t) {

            }
        });
    }

}
