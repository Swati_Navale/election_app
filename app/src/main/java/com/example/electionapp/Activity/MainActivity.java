package com.example.electionapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.TextView;


import com.example.electionapp.BeenClasses.SharedPreferance;
import com.example.electionapp.R;

public class MainActivity extends BaseActivity {

    GridLayout gridLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.custom_bar);
        TextView toolbarTitle = findViewById(R.id.titleText);
        TextView toolbarCounter = findViewById(R.id.titleCounter);
        TextView heading = findViewById(R.id.heading);
        TextView subtitle = findViewById(R.id.subTitle);

        toolbar.setTitle("");
        toolbarTitle.setText("Election App");
        toolbarCounter.setText(" ");
        setSupportActionBar(toolbar);

        String pname = SharedPreferance.getProjectName();
        heading.setText(pname);
        subtitle.setText(SharedPreferance.getProjectTagline());
        gridLayout = findViewById(R.id.grid);
        setSingleEvent(gridLayout);

    }

    private void setSingleEvent(GridLayout gridLayout) {

        for(int i=0;i<gridLayout.getChildCount();i++)
        {
            CardView cardView = (CardView)gridLayout.getChildAt(i);
            final int finalIV = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(finalIV==0)
                    {
                        Intent intent = new Intent(MainActivity.this,Selection_listActivity.class);
                        startActivity(intent);
                    }
                    if(finalIV==1)
                    {
                        Intent intent = new Intent(MainActivity.this, MemberDetailsActivity.class);
                        //intent.putExtra("project_id",project_id);
                        startActivity(intent);
                    }
                    if(finalIV==2)
                    {
                        Intent intent = new Intent(MainActivity.this, MemberRegistrationActivity.class);
                        //intent.putExtra("project_id",project_id);
                        startActivity(intent);
                    }
                    if(finalIV==3)
                    {
                       // Intent intent = new Intent(MainActivity.this,TakteActivity.class);
                       // startActivity(intent);
                    }
                    if(finalIV==4)
                    {
                       // Intent intent = new Intent(MainActivity.this,OtherActivity.class);
                       // startActivity(intent);
                    }
                    if(finalIV==5)
                    {
                       // Intent intent = new Intent(MainActivity.this,LanguageActivity.class);
                       // startActivity(intent);
                    }
                }
            });
        }
    }
}
