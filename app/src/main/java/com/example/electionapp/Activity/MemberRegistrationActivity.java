package com.example.electionapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.electionapp.BeenClasses.Address;
import com.example.electionapp.BeenClasses.MemberData;
import com.example.electionapp.BeenClasses.MemberDetails;
import com.example.electionapp.BeenClasses.SharedPreferance;
import com.example.electionapp.BeenClasses.UserRegisterationResponse;
import com.example.electionapp.Fragments.FragStepOneRegisteration;
import com.example.electionapp.Fragments.FragStepTwoRegisteration;
import com.example.electionapp.Fragments.FragStepThreeRegisteration;
import com.example.electionapp.Interface.ApiClient;
import com.example.electionapp.Interface.ApiInterface;
import com.example.electionapp.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemberRegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnSubmitFragmentOne,btnSubmitFragmentTwo,btnSubmitFragmentThree;
    private String TAG ="MemberRegistrationActivity";
    public static   MemberData memberBean=new MemberData();
    int project_id = SharedPreferance.getProjectId();

    private FragStepOneRegisteration step1_fragment;
    private FragStepTwoRegisteration step2_fragment;
    private FragStepThreeRegisteration step3_fragment;

    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

    MemberDetails.MemberBean memberDetails = new MemberDetails.MemberBean();
    Address.AddressBean addressBean = new Address.AddressBean();

    int progress = 0;
    ProgressBar simpleProgressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_member);
        btnSubmitFragmentOne = findViewById(R.id.btnFragOneId);
        btnSubmitFragmentTwo = findViewById(R.id.btnFragTwoId);
        btnSubmitFragmentThree = findViewById(R.id.btnFragThreeId);
        step1_fragment = new FragStepOneRegisteration();
        fragmentTransaction.add(R.id.registerContainer,step1_fragment);
        fragmentTransaction.commit();
        btnSubmitFragmentOne.setOnClickListener(this);
        btnSubmitFragmentTwo.setOnClickListener(this);
        btnSubmitFragmentThree.setOnClickListener(this);

        simpleProgressBar = (ProgressBar) findViewById(R.id.simpleProgressBar);
       // perform click event on button

    }


   /* private void member_registration() {
        // need to verify the method with actual param's
        Integer roleId = 4;
        String created = "1-1-2000";
        String updated = "2-2-2000";

        ApiInterface apiInterface= ApiClient.getClient().create(ApiInterface.class);
        Call<MemberDetails> call = apiInterface.performRegistration(
                memberBean.getFull_name(),
                memberBean.getMother_name(),
                memberBean.getGender(),
                memberBean.getDate_of_birth(),
                Integer.parseInt(memberBean.getAge()),
                memberBean.getEmail_id(),
                memberBean.getMobile_number1(),
                memberBean.getMobile_number2(),
                memberBean.getAadhar_number(),
                memberBean.getPan_number(),
                Integer.parseInt(memberBean.getCast_id()),
                roleId,
                memberBean.getOccupation(),
                created,
                updated,
                1);//TODO what is p_id
        call.enqueue(new Callback<MemberDetails>() {
            @Override
            public void onResponse(Call<MemberDetails> call, Response<MemberDetails> response) {
                MemberDetails memberDetail=response.body();
                Integer msg=memberDetail.getSuccess();
                if (msg.equals(1)){
                    Toast.makeText(MemberRegistrationActivity.this, "Record inserted", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(MemberRegistrationActivity.this, "Record not inserted", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<MemberDetails> call, Throwable t) {
                Log.e("Error",""+t.getMessage());
            }
        });
    }
*/
    /*
    @author Pk @use of method
    *MemberData Registeration method
     */
    private int doRegisterationProcess(MemberData memberData){
        ApiInterface apiInterface= ApiClient.getClient().create(ApiInterface.class);
        Call<UserRegisterationResponse> call = apiInterface.doRegisteration(memberData);
        call.enqueue(new Callback<UserRegisterationResponse>() {
            @Override
            public void onResponse(Call<UserRegisterationResponse> call, Response<UserRegisterationResponse> response) {
                setProgressValue(progress);
                Intent intent=new Intent(MemberRegistrationActivity.this,MainActivity.class);
                startActivity(intent);
                finish();

            }

            @Override
            public void onFailure(Call<UserRegisterationResponse> call, Throwable t) {
                Log.e("Response msg",""+t.getMessage());
            }
        });

        return 0;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btnFragOneId){
            getFragmentOne();
        }else if(v.getId()==R.id.btnFragTwoId){
            getFragmentTwo();
        }else if(v.getId()==R.id.btnFragThreeId){
            simpleProgressBar.setVisibility(View.VISIBLE);
            getFragmentThree();
        }
    }

    private void getFragmentOne(){

        MemberData memberDtlstemp = FragStepOneRegisteration.getFragmentfirst(memberBean);
        if(memberDtlstemp!=null){
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            memberBean=memberDtlstemp;
            fragmentTransaction.remove(step1_fragment);
            step2_fragment = new FragStepTwoRegisteration();
            fragmentTransaction.add(R.id.registerContainer, step2_fragment);
            fragmentTransaction.commit();
            btnSubmitFragmentOne.setVisibility(View.GONE);
            btnSubmitFragmentTwo.setVisibility(View.VISIBLE);


        }else {
            Toast.makeText(getApplicationContext(),"Some thing went wrong",Toast.LENGTH_LONG).show();
        }


    }
    private void getFragmentTwo(){
        int SUCESS = FragStepTwoRegisteration.getSecondFragment();
        if(SUCESS==1){
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(step2_fragment);
            step3_fragment = new FragStepThreeRegisteration();
            fragmentTransaction.add(R.id.registerContainer, step3_fragment);
            fragmentTransaction.commit();
            btnSubmitFragmentTwo.setVisibility(View.GONE);
            btnSubmitFragmentThree.setVisibility(View.VISIBLE);


        }else if(SUCESS==0){
            Toast.makeText(getApplicationContext(),"Oops not an valid data",Toast.LENGTH_LONG).show();
        }

    }
    private void getFragmentThree(){
        MemberData.AddressBean addressBean = FragStepThreeRegisteration.getThirdFragment();
        try {
            if(addressBean!=null){
                memberBean.setProjectId(Integer.toString(project_id));
                memberBean.setAddress(addressBean);
                memberBean.setRoleId(1);
                memberBean.setCommand("R");
               // member_registration();//TODO Change this Method or remove
                doRegisterationProcess(memberBean);//TODO replace null with actual object
            }else if(addressBean==null){
                Toast.makeText(getApplicationContext(),"Oops not an valid data",Toast.LENGTH_LONG).show();
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    private void setProgressValue(final int progress) {

        simpleProgressBar.setProgress(progress);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setProgressValue(progress + 10);
            }
        });
        thread.start();
    }


   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}

