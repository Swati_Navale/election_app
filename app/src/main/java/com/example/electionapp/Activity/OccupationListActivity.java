package com.example.electionapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.electionapp.Adapter.OccupationAdapter;
import com.example.electionapp.Adapter.OccupationListAdapter;
import com.example.electionapp.BeenClasses.Occupation;
import com.example.electionapp.BeenClasses.OccupationListData;
import com.example.electionapp.BeenClasses.SharedPreferance;
import com.example.electionapp.Interface.ApiClient;
import com.example.electionapp.Interface.ApiInterface;
import com.example.electionapp.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OccupationListActivity extends AppCompatActivity {

    List<Occupation.OccupationsBean> occupationsList =new ArrayList<>();
    EditText searchEd;
    RecyclerView recyclerView;
    OccupationAdapter occupationAdapter;

    int project_id = SharedPreferance.getProjectId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_occupation_list);

        Toolbar toolbar = findViewById(R.id.custom_bar);
        TextView toolbarTitle = findViewById(R.id.titleText);
        toolbar.setTitle("");
        toolbarTitle.setText("Election App");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        searchEd = findViewById(R.id.searchEditText);
        recyclerView = findViewById(R.id.OccupationListView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        occupationAdapter = new OccupationAdapter(this,occupationsList);
        recyclerView.setAdapter(occupationAdapter);


        searchEd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.e("in beforetextchange","Before");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e("in Ontextchange","Ontextchange");
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>=2){
                    getMemberList(s);
                    Log.e("in aftertextchange","afterTextChange:"+s);
                }

            }
        });

        OccupationListData[] occupationListData = new OccupationListData[] {
                new OccupationListData("Engineer",R.drawable.employee),
                new OccupationListData("Doctor", R.drawable.doctor),
                new OccupationListData("Teacher", R.drawable.teacher),
                new OccupationListData("Lawyer", R.drawable.lawyer),
                new OccupationListData("Police", R.drawable.policeman),
                new OccupationListData("BussinessMans", R.drawable.strong),
                new OccupationListData("Chefs", R.drawable.chef)
        };

        RecyclerView recyclerView1 = findViewById(R.id.OccupationList);
        OccupationListAdapter adapter = new OccupationListAdapter(getApplicationContext(),occupationListData);
        recyclerView1.setHasFixedSize(true);
        recyclerView1.setLayoutManager(new LinearLayoutManager(this));
        recyclerView1.setAdapter(adapter);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void getMemberList(CharSequence occupation) {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Occupation> call_member = apiInterface.getOccupationList(occupation,Selection_listActivity.taluka_id,project_id);
        call_member.enqueue(new Callback<Occupation>() {
            @Override
            public void onResponse(Call<Occupation> call, Response<Occupation> response) {
                Occupation member = response.body();

                int msg = member.getSuccess();
                occupationsList.clear();
                if (msg == 1) {
                    occupationsList.addAll(member.getOccupations());

                } else {
                    Toast.makeText(OccupationListActivity.this, "No Member found", Toast.LENGTH_SHORT).show();
                }
                occupationAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Occupation> call, Throwable t) {

            }
        });
    }
}
