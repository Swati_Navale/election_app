package com.example.electionapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.electionapp.Adapter.ListAdapter;
import com.example.electionapp.BeenClasses.District;
import com.example.electionapp.BeenClasses.Member;
import com.example.electionapp.BeenClasses.MemberData;
import com.example.electionapp.BeenClasses.SharedPreferance;
import com.example.electionapp.BeenClasses.State;
import com.example.electionapp.BeenClasses.Taluka;
import com.example.electionapp.Interface.ApiClient;
import com.example.electionapp.Interface.ApiInterface;
import com.example.electionapp.R;
import com.example.electionapp.app.App;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Selection_listActivity extends AppCompatActivity {

    List<State.StateBean> stateRecordList =new ArrayList<>();
    List<District.DistBean> districtrRecordList =new ArrayList<>();
    List<Taluka.TalukaBeen> talukaRecordList =new ArrayList<>();
    List<Member.MembersBean> memberRecordList =new ArrayList<>();
    List<MemberData.AddressBean> addressBeanList = new ArrayList<>();
    TextView count;
    RecyclerView recyclerView;
    ListAdapter listAdapter;
    Button btnMoreFilter;

    ArrayList<String> statelist = new ArrayList<>();
    ArrayList<String> distlist = new ArrayList<>();
    ArrayList<String> talukalist = new ArrayList<>();
    ArrayList<String> memberlist = new ArrayList<>();

    ArrayAdapter<String> stateAdapter,distAdapter,talukaAdapter,arrayAdapter;
    Spinner stateSpinner,distSpinner,talukaSpinner;
    public static String taluka_id;
    int project_id = SharedPreferance.getProjectId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection_list);

        Toolbar toolbar = findViewById(R.id.custom_bar);
        TextView toolbarTitle = findViewById(R.id.titleText);
        toolbar.setTitle("");
        toolbarTitle.setText("Selection List");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        count = findViewById(R.id.count1);
        stateSpinner =  findViewById(R.id.stateSpinner);
        distSpinner = findViewById(R.id.distSpinner);
        talukaSpinner = findViewById(R.id.talukaSpinner);
        stateAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout, R.id.text1, statelist);
        stateSpinner.setAdapter(stateAdapter);
        distAdapter = new ArrayAdapter<String>(this, R.layout.dist_spinner_layout, R.id.text2, distlist);
        distSpinner.setAdapter(distAdapter);
        talukaAdapter = new ArrayAdapter<String>(this, R.layout.taluka_spinner_layout, R.id.text3, talukalist);
        talukaSpinner.setAdapter(talukaAdapter);

       recyclerView = findViewById(R.id.memberlistview1);
       recyclerView.setLayoutManager(new LinearLayoutManager(this));
       listAdapter = new ListAdapter(this,memberRecordList);
       recyclerView.setAdapter(listAdapter);

       btnMoreFilter = findViewById(R.id.btnMoreFilters1);
       btnMoreFilter.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(Selection_listActivity.this, OccupationListActivity.class);
               startActivity(intent);
           }
       });

        getStateList();

        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String state_id=stateRecordList.get(position).getState_id();
                stateSpinner.setSelection(position);
                getDistList(state_id);
                getStateMemberList(state_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        distSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String city_id=districtrRecordList.get(position).getDist_id();
                distSpinner.setSelection(position);
                getTehshilList(city_id);
                getDistMemberList(city_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        talukaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try{
                taluka_id = talukaRecordList.get(position).getTaluka_id();
                talukaSpinner.setSelection(position);
                getTalukaMemberList(taluka_id);}
                catch (Exception e){

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private void getTalukaMemberList(String taluka_id)
    {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Member> call_member = apiInterface.getTalukaMemberList(taluka_id,project_id);
        call_member.enqueue(new Callback<Member>()
        {
            @Override
            public void onResponse(Call<Member> call, Response<Member> response) {
                Member member = response.body();
                count.setText(""+0);
                int msg = member.getSuccess();
                memberRecordList.clear();
                if (msg == 1) {
                    memberRecordList.addAll(member.getMembers());
                    count.setText("" + member.getMembers().size());
                } else {
                    Toast.makeText(Selection_listActivity.this, "No Member found", Toast.LENGTH_SHORT).show();
                }
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Member> call, Throwable t) {

            }
        });
    }

    private void getDistMemberList(String dist_id)
    {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Member> call_member = apiInterface.getDistMemberList(dist_id,project_id);
        call_member.enqueue(new Callback<Member>()
        {
            @Override
            public void onResponse(Call<Member> call, Response<Member> response) {
                Member member = response.body();
                count.setText(""+0);
                int msg = member.getSuccess();
                memberRecordList.clear();
                if (msg == 1) {

                    if(member.getMembers().size()>0){

                    }

                    memberRecordList.addAll(member.getMembers());
                    count.setText("" + member.getMembers().size());
                } else {
                    Toast.makeText(Selection_listActivity.this, "No Member found", Toast.LENGTH_SHORT).show();
                }
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Member> call, Throwable t) {

            }
        });
    }

    private void getStateMemberList(String state_id)
    {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Member> call_member = apiInterface.getStateMemberList(state_id,project_id);
        call_member.enqueue(new Callback<Member>()
        {
            @Override
            public void onResponse(Call<Member> call, Response<Member> response) {
                Member member = response.body();
                count.setText(""+0);
                int msg = member.getSuccess();
                memberRecordList.clear();
                if (msg == 1) {
                    if(member.getMembers().size()>0){
                    }

                    memberRecordList.addAll(member.getMembers());
                    count.setText("" + member.getMembers().size());
                } else {
                    Toast.makeText(Selection_listActivity.this, "No Member found", Toast.LENGTH_SHORT).show();
                }
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Member> call, Throwable t) {

            }
        });
    }

    private void getTehshilList(String dist_id) {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Taluka> call_taluka = apiInterface.getTalukaList(dist_id);
        call_taluka.enqueue(new Callback<Taluka>() {
            @Override
            public void onResponse(Call<Taluka> call, Response<Taluka> response) {

                Taluka taluka = response.body();
                int msg = taluka.getSuccess();
                talukaRecordList.clear();
                talukalist.clear();
                if (msg==1) {

                    talukaRecordList =taluka.getTaluka();
      //              Toast.makeText(Selection_listActivity.this, "Success", Toast.LENGTH_SHORT).show();
                       for (int i = 0; i < talukaRecordList.size(); i++) {
                        Taluka.TalukaBeen taluka_name= talukaRecordList.get(i);
                        talukalist.add(taluka_name.getTaluka_name());

                    }
                    talukaAdapter.notifyDataSetChanged();
                } else {
                    talukalist.clear();
                    talukalist.add("No taluka found");
                    Toast.makeText(Selection_listActivity.this, "No taluka found", Toast.LENGTH_SHORT).show();
                    talukaAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<Taluka> call, Throwable t) {

                Toast.makeText(Selection_listActivity.this, "No data", Toast.LENGTH_SHORT).show();
                Log.e("fail=>", t + " err");
            }
        });
    }

    private void getDistList(String state_id) {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<District> call_city = apiInterface.getDistrictList(state_id);
        call_city.enqueue(new Callback<District>() {
            @Override
            public void onResponse(Call<District> call, Response<District> response) {

                District district = response.body();
                int msg = district.getSuccess();

                districtrRecordList.clear();
                distlist.clear();
                if (msg==1) {

                    districtrRecordList =district.getDist();
        //            Toast.makeText(Selection_listActivity.this, "Success", Toast.LENGTH_SHORT).show();

                    for (int i = 0; i < districtrRecordList.size(); i++) {
                        District.DistBean district_name= districtrRecordList.get(i);
                        distlist.add(district_name.getDist_name());

                    }

                    distAdapter.notifyDataSetChanged();


                } else {
                    distlist.clear();
                    distlist.add("No Distrinct found");
                     Toast.makeText(Selection_listActivity.this, "No district found", Toast.LENGTH_SHORT).show();
                    distAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<District> call, Throwable t) {
                Toast.makeText(Selection_listActivity.this, "No data", Toast.LENGTH_SHORT).show();
                Log.e("fail=>", t + " err");
            }
        });
    }

    private void getStateList() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<State> call_city = apiInterface.getStateList();
        call_city.enqueue(new Callback<State>() {
            @Override
            public void onResponse(Call<State> call, Response<State> response) {

                State state = response.body();
                int msg = state.getSuccess();

                if (msg==1) {
                    stateRecordList =state.getState();
                    statelist.clear();

                    for (int i = 0; i < stateRecordList.size(); i++) {
                        State.StateBean state_name= stateRecordList.get(i);
                        statelist.add(state_name.getState_name());
                    }
                    stateAdapter.notifyDataSetChanged();
                    distAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(Selection_listActivity.this, "No State found", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<State> call, Throwable t) {
                Toast.makeText(Selection_listActivity.this, "No data", Toast.LENGTH_SHORT).show();
                Log.e("fail=>", t + " err");
            }
        });

    }
}


//    protected void onStart()
//    {
//        super.onStart();
//        StateTask stateTask = new StateTask();
//        stateTask.execute();
//    }

//    public class StateTask extends AsyncTask<Void,Void,Void>
//    {
//        ArrayList<String> list;
//        protected void onPreExecute()
//        {
//            super.onPreExecute();
//            list = new ArrayList<>();
//        }
//        @Override
//        protected Void doInBackground(Void... voids) {
//            InputStream is = null;
//            String result = "";
//            String url = "http://192.168.0.104:8080/election_db/state.php";
//            try
//            {
//                HttpClient client = new DefaultHttpClient();
//                HttpPost httpPost = new HttpPost(url);
//                HttpResponse response = client.execute(httpPost);
//                HttpEntity httpEntity = response.getEntity();
//                is = httpEntity.getContent();
//
//                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"));
//
//                String line = "";
//                while ((line = reader.readLine()) != null)
//                {
//                    result += line;
//                    break;
//                }
//                is.close();
//            } catch (Exception e)
//            {
//                Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
//            }
//
//            try {
//
//                JSONArray jsonArray = new JSONArray(result);
//                for(int i=0;i<jsonArray.length();i++)
//                {
//                    JSONObject jsonObject = jsonArray.getJSONObject(i);
//                    list.add(jsonObject.getString("state_name"));
//                }
//
//            }catch (Exception e)
//            {
//                Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
//            }
//            return null;
//        }
//
//        protected void onPostExecute(Void result)
//        {
//            listItems.addAll(list);
//            arrayAdapter.notifyDataSetChanged();
//        }
//    }
//}
