package com.example.electionapp.Adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.electionapp.Interface.ItemClickListner;
import com.example.electionapp.R;

public class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    TextView nameTv,number;
    ItemClickListner itemClickListner;
    public MyHolder(@NonNull View itemView) {
        super(itemView);

        this.nameTv=itemView.findViewById(R.id.nameTv);
        this.number=itemView.findViewById(R.id.numberTv);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        this.itemClickListner.onItemClickListner(v,getLayoutPosition());
    }

    public void setItemClickListner(ItemClickListner icl)
    {
        this.itemClickListner = icl;
    }
}
