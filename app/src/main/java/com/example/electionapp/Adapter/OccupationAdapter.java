package com.example.electionapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.electionapp.BeenClasses.Occupation;
import com.example.electionapp.Interface.ItemClickListner;
import com.example.electionapp.Activity.MemberOccupationActivity;
import com.example.electionapp.R;

import java.util.List;

public class OccupationAdapter extends RecyclerView.Adapter<OccupationHolder> {

    Context context;
    List<Occupation.OccupationsBean> occupation;

    public OccupationAdapter(Context context, List<Occupation.OccupationsBean> occupations) {
        this.context = context;
        this.occupation = occupations;
    }

    @NonNull
    @Override
    public OccupationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.occupation_wise_list,null);
        return new OccupationHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OccupationHolder holder, int position) {

        holder.Occupation.setText(occupation.get(position).getOccupation());

        holder.setItemClickListner(new ItemClickListner() {
            @Override
            public void onItemClickListner(View v, int position) {
                String occupationName = occupation.get(position).getOccupation();
                Intent intent = new Intent(context, MemberOccupationActivity.class);
                intent.putExtra("getOccupation",occupationName);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return occupation.size();
    }
}
