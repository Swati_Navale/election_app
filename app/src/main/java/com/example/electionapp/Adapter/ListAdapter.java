package com.example.electionapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.electionapp.BeenClasses.Member;
import com.example.electionapp.Interface.ItemClickListner;
import com.example.electionapp.Activity.MemberDetailsActivity;
import com.example.electionapp.R;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<MyHolder>{

    Context context;
    List<Member.MembersBean> members;

    public ListAdapter(Context c,List<Member.MembersBean> members)
    {
        this.context = c;
        this.members = members;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row,null);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {

        holder.nameTv.setText(members.get(position).getFull_name());
        holder.number.setText(members.get(position).getOccupation());

        holder.setItemClickListner(new ItemClickListner() {
            @Override
            public void onItemClickListner(View v, int position) {

                String gName = members.get(position).getFull_name();
                String gMName = members.get(position).getMother_name();
                String gGender = members.get(position).getGender();
                String gDob = members.get(position).getDate_of_birth();
                String gAge = members.get(position).getAge();
                String gEmail = members.get(position).getEmail_id();
                String gMob1 = members.get(position).getMobile_number1();
                String gMob2 = members.get(position).getMobile_number2();
                String gAadhar = members.get(position).getAadhar_number();
                String gPan = members.get(position).getPan_number();
                String gOccupation = members.get(position).getOccupation();
                String gCast = members.get(position).getCast_id();
                String gRole = members.get(position).getRole_id();
                String gCreated = members.get(position).getCreatedOn();
                String gUpdated = members.get(position).getUpdatedOn();
                String m_id = members.get(position).getM_id();

                Intent intent = new Intent(context, MemberDetailsActivity.class);
                intent.putExtra("sName",gName);
                intent.putExtra("sMName",gMName);
                intent.putExtra("sGender",gGender);
                intent.putExtra("sDob",gDob);
                intent.putExtra("sAge",gAge);
                intent.putExtra("sEmail",gEmail);
                intent.putExtra("sMob1",gMob1);
                intent.putExtra("sMob2",gMob2);
                intent.putExtra("sAadhar",gAadhar);
                intent.putExtra("sPan",gPan);
                intent.putExtra("sOccupation",gOccupation);
                intent.putExtra("sCast",gCast);
                intent.putExtra("sRole",gRole);
                intent.putExtra("sCreated",gCreated);
                intent.putExtra("sUpdated",gUpdated);

                context.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return members.size();
    }
}
