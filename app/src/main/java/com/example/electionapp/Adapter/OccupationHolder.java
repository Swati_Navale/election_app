package com.example.electionapp.Adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.electionapp.Interface.ItemClickListner;
import com.example.electionapp.R;

public class OccupationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView Occupation;
    ItemClickListner itemClickListner;


    public OccupationHolder(@NonNull View itemView) {
        super(itemView);
        this.Occupation=itemView.findViewById(R.id.tvOccupation);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        this.itemClickListner.onItemClickListner(v,getLayoutPosition());
    }

    public void setItemClickListner(ItemClickListner icl)
    {
        this.itemClickListner = icl;
    }
}
