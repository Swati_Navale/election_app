package com.example.electionapp.Adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.electionapp.Interface.ItemClickListner;
import com.example.electionapp.R;

public class MemberOccupationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView nameTv,occupation;
    ItemClickListner itemClickListner;

    public MemberOccupationHolder(@NonNull View itemView) {
        super(itemView);

            this.nameTv=itemView.findViewById(R.id.fullnameTv);
            this.occupation=itemView.findViewById(R.id.occupTv);
            itemView.setOnClickListener(this);
        }

    @Override
    public void onClick(View v) {
        this.itemClickListner.onItemClickListner(v,getLayoutPosition());
    }

    public void setItemClickListner(ItemClickListner icl)
    {
        this.itemClickListner = icl;
    }
}
