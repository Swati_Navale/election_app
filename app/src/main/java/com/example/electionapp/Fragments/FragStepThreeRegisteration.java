package com.example.electionapp.Fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.electionapp.BeenClasses.MemberData;
import com.example.electionapp.Interface.ApiClient;
import com.example.electionapp.BeenClasses.Address;
import com.example.electionapp.BeenClasses.District;
import com.example.electionapp.BeenClasses.MemberDetails;
import com.example.electionapp.BeenClasses.State;
import com.example.electionapp.BeenClasses.Taluka;
import com.example.electionapp.Activity.MemberRegistrationActivity;
import com.example.electionapp.Interface.ApiInterface;
import com.example.electionapp.R;
import com.example.electionapp.Activity.Selection_listActivity;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragStepThreeRegisteration extends Fragment {

    private List<State.StateBean> stateRecordList = new ArrayList<>();
    private List<District.DistBean> districtrRecordList = new ArrayList<>();
    private List<Taluka.TalukaBeen> talukaRecordList = new ArrayList<>();

    private ArrayList<String> statelist = new ArrayList<>();
    private ArrayList<String> distlist = new ArrayList<>();
    private ArrayList<String> talukalist = new ArrayList<>();

    private ArrayAdapter<String> stateAdapter, distAdapter, talukaAdapter;
    private static Spinner stateSpinner, distSpinner, talukaSpinner;
    private static  TextInputEditText addr,villageEditText,zipCode;
//    Button btn;
    private static String address,state,district,taluka,village,zipcode;
    private static String taluka_id,state_id,district_id;

    public FragStepThreeRegisteration() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_register__step3_, container, false);

        addr = v.findViewById(R.id.address);
        villageEditText = v.findViewById(R.id.village);
        zipCode = v.findViewById(R.id.zipcode);
        stateSpinner = v.findViewById(R.id.stateSpinner);
        distSpinner = v.findViewById(R.id.distSpinner);
        talukaSpinner = v.findViewById(R.id.talukaSpinner);
        stateAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_layout, R.id.text1, statelist);
        stateSpinner.setAdapter(stateAdapter);
        distAdapter = new ArrayAdapter<String>(getContext(), R.layout.dist_spinner_layout, R.id.text2, distlist);
        distSpinner.setAdapter(distAdapter);
        talukaAdapter = new ArrayAdapter<String>(getContext(), R.layout.taluka_spinner_layout, R.id.text3, talukalist);
        talukaSpinner.setAdapter(talukaAdapter);

        Toolbar toolbar = v.findViewById(R.id.custom_bar);
        TextView toolbarTitle = v.findViewById(R.id.titleText);

        toolbar.setTitle("");
        toolbarTitle.setText("Registration Step 3");
        toolbar.setNavigationIcon(R.drawable.arrow_left_circle);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        getStateList();

        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                 state_id = stateRecordList.get(position).getState_id();
               // Toast.makeText(getContext(), "State_Id __-" + state_id, Toast.LENGTH_SHORT).show();
                stateSpinner.setSelection(position);

                getDistList(state_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        distSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                 district_id = districtrRecordList.get(position).getDist_id();
               // Toast.makeText(getContext(), "Dist_Id __-" + city_id, Toast.LENGTH_SHORT).show();
                distSpinner.setSelection(position);

                getTehshilList(district_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        talukaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                 taluka_id = talukaRecordList.get(position).getTaluka_id();
                talukaSpinner.setSelection(position);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                address=addr.getText().toString();
//                state = stateSpinner.getSelectedItem().toString() ;
//                district = distSpinner.getSelectedItem().toString() ;
//                taluka = talukaSpinner.getSelectedItem().toString() ;
//                village=villageEditText.getText().toString();
//                zipcode=zipCode.getText().toString();
//                MemberRegistrationActivity mainActivity = (MemberRegistrationActivity) getActivity();
//                mainActivity.form3Vailadation(address,state,district,taluka,village,zipcode);
//            }
//        });

        return v;
    }

    public static MemberData.AddressBean getThirdFragment()
    {
        MemberData.AddressBean  addressBean=new MemberData.AddressBean();



        if(isVaild()) {
            address=addr.getText().toString();
            state = stateSpinner.getSelectedItem().toString() ;
            district = distSpinner.getSelectedItem().toString() ;
            taluka = talukaSpinner.getSelectedItem().toString() ;
            village=villageEditText.getText().toString();
            zipcode=zipCode.getText().toString();

            addressBean.setCountry_id(1);
            addressBean.setState_id(Integer.parseInt(state_id));
            addressBean.setDist_id(Integer.parseInt(district_id));
            addressBean.setTaluka_id(Integer.parseInt(taluka_id));
            addressBean.setVillage(village);
            addressBean.setZipcode(zipcode);
            //MemberRegistrationActivity.memberBean.setMobile_number1(mob1);

            return addressBean;
        }{
        Log.e("step two","Enter valid data");
    }
        return null;
    }

    private static  boolean isVaild(){
        Boolean isValid=Boolean.FALSE;
        if(addr.getText().toString().isEmpty()){
            addr.setError("Enter Email_id");
            return false;
        }
        if(villageEditText.getText().toString().isEmpty()){
            villageEditText.setError("Enter name");
            return false;
        }
        if(zipCode.getText().toString().isEmpty()){
            zipCode.setError("Enter mother name");
            return false;
        }
        else
        {
            isValid=Boolean.TRUE;
        }
        return isValid;
    }



    private void getTehshilList(String dist_id) {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Taluka> call_taluka = apiInterface.getTalukaList(dist_id);
        call_taluka.enqueue(new Callback<Taluka>() {
            @Override
            public void onResponse(Call<Taluka> call, Response<Taluka> response) {

                Taluka taluka = response.body();
                int msg = taluka.getSuccess();

                if (msg == 1) {

                    talukaRecordList = taluka.getTaluka();
                    talukalist.clear();
                   // talukalist.add("Select Taluka");
                    for (int i = 0; i < talukaRecordList.size(); i++) {
                        Taluka.TalukaBeen taluka_name = talukaRecordList.get(i);
                        talukalist.add(taluka_name.getTaluka_name());

                    }

                    talukaAdapter.notifyDataSetChanged();


                } else {

                    Toast.makeText(getContext(), "No city found", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Taluka> call, Throwable t) {

                Toast.makeText(getContext(), "No data", Toast.LENGTH_SHORT).show();
                Log.e("fail=>", t + " err");


            }
        });


    }

    private void getDistList(String state_id) {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<District> call_city = apiInterface.getDistrictList(state_id);
        call_city.enqueue(new Callback<District>() {
            @Override
            public void onResponse(Call<District> call, Response<District> response) {

                District district = response.body();
                int msg = district.getSuccess();

                if (msg == 1) {

                    districtrRecordList = district.getDist();
                    distlist.clear();
                  //  distlist.add("Select District");
                    for (int i = 0; i < districtrRecordList.size(); i++) {
                        District.DistBean district_name = districtrRecordList.get(i);
                        distlist.add(district_name.getDist_name());

                    }

                    distAdapter.notifyDataSetChanged();


                } else {

                    Toast.makeText(getContext(), "No city found", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<District> call, Throwable t) {

                Toast.makeText(getContext(), "No data", Toast.LENGTH_SHORT).show();
                Log.e("fail=>", t + " err");


            }
        });


    }

    private void getStateList() {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<State> call_city = apiInterface.getStateList();
        call_city.enqueue(new Callback<State>() {
            @Override
            public void onResponse(Call<State> call, Response<State> response) {

                State state = response.body();
                int msg = state.getSuccess();

                if (msg == 1) {

                    stateRecordList = state.getState();
                    statelist.clear();
                   // statelist.add("Select State");
                    for (int i = 0; i < stateRecordList.size(); i++) {
                        State.StateBean state_name = stateRecordList.get(i);
                        statelist.add(state_name.getState_name());

                    }

                    stateAdapter.notifyDataSetChanged();

                } else {

                    Toast.makeText(getContext(), "No city found", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<State> call, Throwable t) {

                Toast.makeText(getContext(), "No data", Toast.LENGTH_SHORT).show();
                Log.e("fail=>", t + " err");


            }
        });

    }


}