package com.example.electionapp.Fragments;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.electionapp.BeenClasses.MemberData;
import com.example.electionapp.Interface.ApiClient;
import com.example.electionapp.BeenClasses.Cast;
import com.example.electionapp.BeenClasses.MemberDetails;
import com.example.electionapp.Interface.ApiInterface;
import com.example.electionapp.R;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class  FragStepOneRegisteration extends Fragment {

    public static String name,mother,age,gender,dob,cast;
    private static TextInputEditText tInFullName, tInMotherName, tInAge, tInGender;
    public  static TextInputEditText tIndob;
    private static  Spinner castSpinner;
    private DatePickerDialog datePickerDialog;

    private List<Cast.CastBean> castRecordList = new ArrayList<>();
    private ArrayList<String> castlist = new ArrayList<>();
    private ArrayAdapter<String> castAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register__step1_, container, false);

        castSpinner = v.findViewById(R.id.castSpinner);
        tInFullName = v.findViewById(R.id.fullname);
        tInMotherName = v.findViewById(R.id.mothername);
        tInGender = v.findViewById(R.id.gender);
        tIndob = v.findViewById(R.id.dob);

        Toolbar toolbar = v.findViewById(R.id.custom_bar);
        TextView toolbarTitle = v.findViewById(R.id.titleText);
        toolbar.setTitle("");
        toolbarTitle.setText("Registration Form");
        toolbar.setNavigationIcon(R.drawable.arrow_left_circle);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        tIndob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectDateFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }

        });

        castSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String cast_id = castRecordList.get(position).getCast_id();
//                Toast.makeText(getContext(), "Cast_Id : " + cast_id, Toast.LENGTH_SHORT).show();
                castSpinner.setSelection(position);

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return v;
    }

    public static MemberData getFragmentfirst(MemberData bn){



        if(isVaild()){

            name= tInFullName.getText().toString();
            mother= tInMotherName.getText().toString();
          //  age= tInAge.getText().toString();
            dob= tIndob.getText().toString();
            gender= tInGender.getText().toString();
            cast = String.valueOf(castSpinner.getSelectedItemPosition()) ;

            bn.setFullName(name);
            bn.setMotherName(mother);
            //bn.setAge(age);
            bn.setGender(gender);
            bn.setDateOfBirth(dob);
            bn.setCastId(Integer.parseInt(cast));
            return  bn;
        }{
           Log.e("step one","Enter vaold data");
        }

        return null;
    }

    private static boolean isVaild(){
        Boolean isValid=Boolean.FALSE;
        if(tInFullName.getText().toString().isEmpty()){
            tInFullName.setError("Enter name");
            return false;
        }
        if(tInMotherName.getText().toString().isEmpty()){
            tInMotherName.setError("Enter mother name");
            return false;
        }
        if(tIndob.getText().toString().isEmpty()){
            tIndob.setError("Plz select your bithdate");
            return false;
        }
        if(tInGender.getText().toString().isEmpty()){
            tInGender.setError("enter gender");
            return false;
        }
        else
        {
            isValid=Boolean.TRUE;
        }
        return isValid;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        castAdapter = new ArrayAdapter<String>(getContext(), R.layout.cast_spinner_layout, R.id.text, castlist);
        castSpinner.setAdapter(castAdapter);
        getCastList();
    }

    public static class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        @NonNull
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int date = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, date);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            return dialog;
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            Calendar calendar = Calendar.getInstance();

            calendar.set(Calendar.YEAR, yy);
            calendar.set(Calendar.MONTH, mm);
            calendar.set(Calendar.DAY_OF_MONTH, dd);

            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            tIndob.setText(year + "-" + month + "-" + day);
        }
    }

    private void getCastList() {

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Cast> call_cast = apiInterface.getCastList();
        call_cast.enqueue(new Callback<Cast>() {
            @Override
            public void onResponse(Call<Cast> call, Response<Cast> response) {

                Cast cast = response.body();
                int msg = cast.getSuccess();

                try {
                    if (msg == 1) {
                        castRecordList = cast.getCast();
                        castlist.clear();
                        for (int i = 0; i < castRecordList.size(); i++) {
                            Cast.CastBean cast_name = castRecordList.get(i);
                            castlist.add(cast_name.getName());
                        }
                        castAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getContext(), "No city found", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<Cast> call, Throwable t) {

                Toast.makeText(getContext(), "No data", Toast.LENGTH_SHORT).show();
                Log.e("fail=>", t + " err");
            }
        });
    }
}
