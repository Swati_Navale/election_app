package com.example.electionapp.Fragments;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.electionapp.Activity.MemberRegistrationActivity;
import com.example.electionapp.BeenClasses.MemberDetails;
import com.example.electionapp.R;
import com.google.android.material.textfield.TextInputEditText;

public class FragStepTwoRegisteration extends Fragment {

    private static TextInputEditText iInEmail,iInPassword,mobile1,mobile2,aadharNumber,panNumber,occupation;
    private static String email_id,pwd,mob1,mob2,a_number,pan_number,occup;

    public FragStepTwoRegisteration() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_register__step2_, container, false);

        iInEmail = v.findViewById(R.id.email);
        iInPassword = v.findViewById(R.id.password);
        mobile1 = v.findViewById(R.id.mob1);
        mobile2 = v.findViewById(R.id.mob2);
        aadharNumber = v.findViewById(R.id.aadhar_no);
        panNumber = v.findViewById(R.id.pan_no);
        occupation = v.findViewById(R.id.occupation);

        Toolbar toolbar = v.findViewById(R.id.custom_bar);
        TextView toolbarTitle = v.findViewById(R.id.titleText);

        toolbar.setTitle("");
        toolbarTitle.setText("Registration Step 2");
        toolbar.setNavigationIcon(R.drawable.arrow_left_circle);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return v;
    }

    public static int  getSecondFragment()
    {
        email_id= iInEmail.getText().toString();
        pwd = iInPassword.getText().toString();
        mob1=mobile1.getText().toString();
        mob2=mobile2.getText().toString();
        a_number=aadharNumber.getText().toString();
        pan_number=panNumber.getText().toString();
        occup= occupation.getText().toString();

        if(isVaild()) {
            MemberRegistrationActivity.memberBean.setEmailId(email_id);
            MemberRegistrationActivity.memberBean.setPassword(pwd);
            MemberRegistrationActivity.memberBean.setMobileNumber1(mob1);
            MemberRegistrationActivity.memberBean.setMobileNumber2(mob2);
            MemberRegistrationActivity.memberBean.setAdharNumber(a_number);
            MemberRegistrationActivity.memberBean.setPanNumber(pan_number);
            MemberRegistrationActivity.memberBean.setOccupation(occup);
            return 1;
        }{
        Log.e("step two","Enter valid data");
    }
        return 0;
    }

    private static boolean isVaild() {

        Boolean isValid=Boolean.FALSE;
        if(iInEmail.getText().toString().isEmpty()){
            iInEmail.setError("Enter Email_id");
            return false;
        }
        if(iInPassword.getText().toString().isEmpty()){
            iInPassword.setError("Enter name");
            return false;
        }
        if(mobile1.getText().toString().isEmpty()){
            mobile1.setError("Enter mother name");
            return false;
        }
        if(aadharNumber.getText().toString().isEmpty()){
            aadharNumber.setError("Plz select your bithdate");
            return false;
        }
        if(panNumber.getText().toString().isEmpty()){
            panNumber.setError("enter gender");
            return false;
        }
        if(occupation.getText().toString().isEmpty()){
            occupation.setError("enter gender");
            return false;
        }
        else
        {
            isValid=Boolean.TRUE;
        }
        return isValid;
    }
}




