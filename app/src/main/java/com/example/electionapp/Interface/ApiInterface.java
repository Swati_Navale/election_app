package com.example.electionapp.Interface;

import com.example.electionapp.BeenClasses.Cast;
import com.example.electionapp.BeenClasses.Change_Password;
import com.example.electionapp.BeenClasses.Check_Otp;
import com.example.electionapp.BeenClasses.District;
import com.example.electionapp.BeenClasses.Member;
import com.example.electionapp.BeenClasses.MemberData;
import com.example.electionapp.BeenClasses.MemberDetails;
import com.example.electionapp.BeenClasses.MobileOtp;
import com.example.electionapp.BeenClasses.Occupation;
import com.example.electionapp.BeenClasses.OccupationWiseMember;
import com.example.electionapp.BeenClasses.Project;
import com.example.electionapp.BeenClasses.State;
import com.example.electionapp.BeenClasses.Taluka;
import com.example.electionapp.BeenClasses.User;
import com.example.electionapp.BeenClasses.UserRegisterationResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    //@FormUrlEncoded
    @POST("cast.php")
    Call<Cast> getCastList();

    @POST("state.php")
    Call<State> getStateList();

    @FormUrlEncoded
    @POST("district.php")
    Call<District> getDistrictList(@Field("state_id") String state_id);

    @FormUrlEncoded
    @POST("taluka.php")
    Call<Taluka> getTalukaList(@Field("dist_id") String dist_id);

    @FormUrlEncoded
    @POST("taluka_member_list.php")
    Call<Member> getTalukaMemberList(@Field("taluka_id") String taluka_id,@Field("p_id") Integer p_id);

    @FormUrlEncoded
    @POST("dist_member_list.php")
    Call<Member> getDistMemberList(@Field("dist_id") String dist_id,@Field("p_id") Integer p_id);

    @FormUrlEncoded
    @POST("state_member_list.php")
    Call<Member> getStateMemberList(@Field("state_id") String state_id,@Field("p_id") Integer p_id);

    @FormUrlEncoded
    @POST("member.php")
     Call<MemberDetails> getMember(@Field("m_id") String m_id,@Field("p_id") Integer p_id);

    @FormUrlEncoded
    @POST("registration.php")
    Call<MemberDetails> performRegistration(@Field("fullName") String fullName,@Field("motherName") String motherName,@Field("gender") String gender,@Field("dateOfBirth") String dateOfBirth,@Field("age") Integer age,@Field("emailId") String emailId,@Field("mobileNumber1") String mobileNumber1,@Field("mobileNumber2") String mobileNumber2,@Field("adharNumber") String adharNumber,@Field("panNumber") String panNumber,@Field("castId") Integer castId,@Field("roleId") Integer roleId,@Field("occupation") String occupation,@Field("created") String created,@Field("updated") String updated,@Field("project_id") Integer p_id);

    @FormUrlEncoded
    @POST("occupation.php")
    Call<Occupation> getOccupationList(@Field("occupation") CharSequence occupation,@Field("t_id") String t_id,@Field("p_id") Integer p_id);

    @FormUrlEncoded
    @POST("member_occupation_list.php")
    Call<OccupationWiseMember> getOccupationMemberList(@Field("occupation") String occupation,@Field("t_id") String t_id,@Field("p_id") Integer p_id);

    @FormUrlEncoded
    @POST("project.php")
    Call<Project> getProject(@Field("p_name") String p_name);

    @FormUrlEncoded
    @POST("forgot_password.php")
    Call<User> getEmail( @Field("email_id") String email);

    @FormUrlEncoded
    @POST("forgotpassword_withotp.php")
    Call<MobileOtp> getOtp(@Field("mobile_number") String mobile);

    @FormUrlEncoded
    @POST("check_otp.php")
    Call<Check_Otp> getCheckedOtp(@Field("user_otp") String otp);

    @FormUrlEncoded
    @POST("set_new_password.php")
    Call<Change_Password> changePassword(@Field("password") String password, @Field("contact") String contact);

    @FormUrlEncoded
    @POST("login.php")
    Call<User> checkUserPassword(@Field("email") String email, @Field("password") String password);

    @POST("master_member.php")
    Call<UserRegisterationResponse> doRegisteration(@Body MemberData memberData);//insert data using pojo class

}
